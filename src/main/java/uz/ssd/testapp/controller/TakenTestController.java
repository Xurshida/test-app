package uz.ssd.testapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.ssd.testapp.entity.User;
import uz.ssd.testapp.payload.ReqAnswer;
import uz.ssd.testapp.payload.ReqTakenTest;
import uz.ssd.testapp.security.CurrentUser;
import uz.ssd.testapp.service.TakenTestService;
import uz.ssd.testapp.utils.AppConstants;

import java.util.List;

@RestController
@RequestMapping("/api/takenTest")
public class TakenTestController {
    @Autowired
    TakenTestService takenTestService;

    @PostMapping
    public HttpEntity<?> saveOrEdit(@RequestBody ReqTakenTest reqTakenTest, @CurrentUser User user) {
        return ResponseEntity.ok().body(takenTestService.saveOrEdit(reqTakenTest, user));
    }

    @PostMapping("/{id}")
    public HttpEntity<?> saveAnswersOne(@RequestBody List<ReqAnswer> answerList, @PathVariable Long id) {
        return ResponseEntity.ok().body(takenTestService.saveAnswersOne(answerList, id));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> saveAnswersTwo(@RequestBody List<ReqAnswer> answerList, @PathVariable Long id) {
        return ResponseEntity.ok().body(takenTestService.saveAnswersTwo(answerList, id));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getTakenTest(@PathVariable Long id) {
        return ResponseEntity.ok().body(takenTestService.getTakenTest(id));
    }

    @GetMapping
    public HttpEntity<?> getTakenTests(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                       @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok().body(takenTestService.getTakenTests(page, size));
    }

}
