package uz.ssd.testapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ssd.testapp.service.AnswerService;

@RestController
@RequestMapping("/answer")
public class AnswerController {
    @Autowired
    AnswerService answerService;

    @GetMapping("/{id}")
    public HttpEntity<?> getAnswer(@PathVariable Long id){
        return ResponseEntity.ok().body(answerService.getAnswer(id));
    }

    @GetMapping("/{takenTestId}")
    public HttpEntity<?> getAnswers(@PathVariable Long takenTestId){
        return ResponseEntity.ok().body(answerService.getAnswers(takenTestId));
    }

}
