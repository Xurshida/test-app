package uz.ssd.testapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.ssd.testapp.entity.User;
import uz.ssd.testapp.entity.enums.RoleName;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.ReqUser;
import uz.ssd.testapp.security.CurrentUser;
import uz.ssd.testapp.service.UserService;
import uz.ssd.testapp.utils.AppConstants;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    UserService userService;

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @PutMapping("/blockOrUnblock/{id}")
    public ApiResponseModel blockOrUnblockUser(@PathVariable("id") Long id) {
        return userService.blockOrUnblock(id);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @GetMapping("/{id}")
    public HttpEntity<?> getUser(@PathVariable Long id){
        return ResponseEntity.ok().body(userService.getUser(id));
    }

    @PutMapping("/profile")
    public ApiResponseModel changeProfile(@RequestBody ReqUser reqUser, @CurrentUser User user) {
        return userService.changeProfileSettings(reqUser, user);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')")
    @GetMapping
    public HttpEntity<?> getUsers(@RequestParam(name = "startDate",defaultValue = AppConstants.BEGIN_DATE) String startTime,
                                       @RequestParam(name = "endDate",defaultValue = AppConstants.END_DATE) String endTime,
                                       @RequestParam(name = "roleName") String roleName,
                                       @RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                       @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size,
                                       @RequestParam(name = "search", defaultValue = "all") String search) {
        ApiResponseModel response = userService.getAllUsers(startTime, endTime, RoleName.valueOf(roleName), page, size, search);
        return ResponseEntity.ok().body(response);
    }

}
