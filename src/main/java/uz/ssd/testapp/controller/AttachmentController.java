package uz.ssd.testapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.repository.AttachmentRepository;
import uz.ssd.testapp.service.AttachmentService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/api/attach")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @Autowired
    AttachmentRepository attachmentRepository;

    @PostMapping("/upload")
    public HttpEntity<?> uploadFile(MultipartHttpServletRequest request) throws IOException {
        ApiResponseModel apiResponseModel = attachmentService.uploadFile(request);
        return ResponseEntity.ok().body(apiResponseModel);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getFile(@PathVariable Long id, HttpServletResponse response) {
        return attachmentService.getAttachmentContent(id, response);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteFile(@PathVariable Long id) {
        ApiResponseModel apiResponse = attachmentService.deleteAttachment(id);
        return ResponseEntity.ok().body(apiResponse);
    }
}
