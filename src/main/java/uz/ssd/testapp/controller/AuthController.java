package uz.ssd.testapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;
import uz.ssd.testapp.entity.enums.RoleName;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.ReqSignIn;
import uz.ssd.testapp.payload.ReqSignUp;
import uz.ssd.testapp.repository.UserRepository;
import uz.ssd.testapp.security.AuthService;
import uz.ssd.testapp.security.JwtTokenProvider;
import uz.ssd.testapp.service.MailService;

import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

   @Autowired
   JwtTokenProvider jwtTokenProvider;

   @Autowired
   AuthenticationManager authenticate;

   @Autowired
   AuthService authService;

   @Autowired
   UserRepository userRepository;


   @PreAuthorize("hasAnyAuthority('ROLE_SUPER_ADMIN')")
   @PostMapping("/register")
   public HttpEntity<?> register(@RequestBody ReqSignUp reqSignUp, @RequestParam(name = "role") String roleName) throws Exception {
      ApiResponseModel response = authService.register(reqSignUp, roleName.toLowerCase());
      if (response.getStatusCode()==201) {
         return ResponseEntity
                 .status(HttpStatus.CREATED)
                 .body(response);
      }
      return ResponseEntity
              .status(HttpStatus.CONFLICT)
              .body(response);

   }


   @PostMapping("/login")
   public HttpEntity<?> login(@RequestBody ReqSignIn reqSignIn) {
      return ResponseEntity.ok(authService.getApiToken(reqSignIn.getEmail(), reqSignIn.getPassword()));
   }
}
