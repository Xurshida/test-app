package uz.ssd.testapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.ssd.testapp.entity.User;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.ReqQuestion;
import uz.ssd.testapp.payload.ReqTest;
import uz.ssd.testapp.security.CurrentUser;
import uz.ssd.testapp.service.TestService;

import java.util.List;

@RestController
@RequestMapping("/api/test")
public class TestController {

    @Autowired
    TestService testService;

    //**************** SAVE TEST ****************//
    @PostMapping("/saveOrEdit")
    public ApiResponseModel saveOrEditTest(@RequestBody ReqTest reqTest){
        return testService.saveOrEdit(reqTest);
    }

    //**************** SAVE QUESTION ****************//
    @PostMapping("/saveQuestionOne/{id}")
    public ApiResponseModel saveQuestionOne(@RequestBody List<ReqQuestion> reqQuestions,@PathVariable Long id){
        return testService.saveQuestionOne(reqQuestions,id);
    }

    //**************** SAVE QUESTION ****************//
    @PostMapping("/saveQuestionTwo/{id}")
    public ApiResponseModel saveOrEditQuestion(@RequestBody List<ReqQuestion> reqQuestions,@PathVariable Long id){
        return testService.saveQuestionTwo(reqQuestions,id);
    }

    //**************** GET TEST ****************//
    @GetMapping("/getTests")
    public ApiResponseModel getTest(@CurrentUser User user){
        return testService.getTests(user);
    }

    //**************** GET TEST ****************//
    @GetMapping("/getTestsQuestionOne/{id}")
    public ApiResponseModel getTestsQuestionOne(@PathVariable Long id,@CurrentUser User user){
        return testService.getTestsQuestionOne(id,user);
    }

    //**************** GET TEST ****************//
    @GetMapping("/getTestsQuestionTwo/{id}")
    public ApiResponseModel getTestsQuestionTwo(@PathVariable Long id,@CurrentUser User user){
        return testService.getTestsQuestionTwo(id,user);
    }

    //**************** GET TEST ****************//
    @PreAuthorize("hasAnyAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/delete/{id}")
    public ApiResponseModel deleteTest(@PathVariable Long id){
        return testService.deleteTest(id);
    }

    //**************** GET TEST ****************//
    @PreAuthorize("hasAnyAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/permission/{testId}/{userId}")
    public ApiResponseModel permission(@PathVariable Long testId,@PathVariable Long userId){
        return testService.permission(testId,userId);
    }

    //**************** GET TEST ****************//
    @PreAuthorize("hasAnyAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/unPermission/{testId}/{userId}")
    public ApiResponseModel unPermission(@PathVariable Long testId,@PathVariable Long userId){
        return testService.unPermission(testId,userId);
    }

    //**************** GET TEST ****************//
    @PreAuthorize("hasAnyAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/changeQuestion")
    public ApiResponseModel changeQuestion( @RequestBody ReqQuestion question){
        return testService.changeQuestion(question);
    }

}
