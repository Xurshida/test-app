package uz.ssd.testapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.ssd.testapp.payload.ReqSignUp;
import uz.ssd.testapp.payload.ReqVariable;
import uz.ssd.testapp.service.UserService;
import uz.ssd.testapp.service.VariableService;

@RestController
@PreAuthorize("hasAnyAuthority('ROLE_SUPER_ADMIN')")
@RequestMapping("/api")
public class SuperAdminController {

    @Autowired
    UserService userService;

    @Autowired
    VariableService variableService;

    @PostMapping("/admin/blockOrUnblock/{id}")
    public HttpEntity<?> blockOrUnblockAdmin(@PathVariable Long id) {
        return ResponseEntity.ok().body(userService.blockOrUnblockAdmin(id));
    }

    @GetMapping("/admin/{id}")
    public HttpEntity<?> getAdmin(@PathVariable Long id){
        return ResponseEntity.ok().body(userService.getAdmin(id));
    }


    @DeleteMapping("/admin/{id}")
    public HttpEntity<?> deleteAdmin(@PathVariable Long id){
        return ResponseEntity.ok().body(userService.deleteAdmin(id));
    }

    @GetMapping("/variables")
    public HttpEntity<?> getVariant(){
        return ResponseEntity.ok().body(variableService.getVariable());
    }

    @PostMapping("/variables")
    public HttpEntity<?> changeVariable(@RequestBody ReqVariable reqVariable){
        return ResponseEntity.ok().body(variableService.changeVariable(reqVariable));
    }

}
