package uz.ssd.testapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ssd.testapp.entity.Question;
import uz.ssd.testapp.entity.Variant;

public interface QuestionRepository extends JpaRepository<Question, Long> {

}
