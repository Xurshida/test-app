package uz.ssd.testapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.ssd.testapp.collection.UserCol;
import uz.ssd.testapp.entity.Role;
import uz.ssd.testapp.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> getByPhoneNumber(String phoneNumber);

    Optional<User> getByEmail(String email);

    Optional<User> getByPassportNumber(String passportNumber);

    boolean existsByPhoneNumber(String phoneNumber);

    @Query(value = "select * from users u where u.id in(select user_id from user_role ur where ur.role_id = :roleId " +
            "intersect select id from users u where u.created_at between cast(:startTime as timestamp) and cast(:endTime as timestamp))"
            ,nativeQuery = true)
    Page<UserCol> findAllByCreatedAtBetweenAndRolesQuery(@Param(value = "startTime") String start,
                                                       @Param("endTime") String end,
                                                       @Param("roleId") Integer id,
                                                       Pageable pageable);

    @Query(value = "select * from users u where u.id in \n" +
            "(select user_id from user_role ur where ur.role_id = :roleId " +
            "intersect " +
            "select id from users u where u.created_at between :startTime and :endTime" +
            "and (position(:search in users.first_name) > 0 or position(:search in users.last_name) > 0)",
            nativeQuery = true)
    Page<UserCol> findAllByCreatedAtBetweenAndRolesContainsQuery(@Param("startTime") String start,
                                                               @Param("endTime") String end,
                                                               @Param("roleId") Integer id,
                                                               @Param("search") String search,
                                                               Pageable pageable);

    @Query(value = "select count(u.id) from users u where u.id in \n" +
            "(select user_id from user_role ur where ur.role_id = :roleId)", nativeQuery = true)
    Integer countAllByRoles(@Param("roleId") Integer id);
}
