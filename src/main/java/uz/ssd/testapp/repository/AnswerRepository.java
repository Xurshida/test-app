package uz.ssd.testapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ssd.testapp.collection.AnswerCol;
import uz.ssd.testapp.entity.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Long> {

}
