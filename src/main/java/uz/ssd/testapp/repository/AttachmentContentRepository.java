package uz.ssd.testapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ssd.testapp.entity.AttachmentContent;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, Long> {
}
