package uz.ssd.testapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ssd.testapp.entity.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {

}
