package uz.ssd.testapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.ssd.testapp.collection.TakenTestCol;
import uz.ssd.testapp.entity.TakenTest;

import java.util.Optional;

public interface TakenTestRepository extends JpaRepository<TakenTest, Long> {
//    @Query(value = "select id, total_score, test_id, user_id from taken_test", nativeQuery = true)
    Page<TakenTestCol> findAllBy(Pageable pageable);

    Optional<TakenTestCol> getById(Long id);
}
