package uz.ssd.testapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ssd.testapp.entity.Role;
import uz.ssd.testapp.entity.enums.RoleName;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    List<Role> findAllByNameIn(List<RoleName> asList);

    Optional<Role> findByName(RoleName roleAdmin);

    List<Role> findAllByName(RoleName roleSuperAdmin);
}
