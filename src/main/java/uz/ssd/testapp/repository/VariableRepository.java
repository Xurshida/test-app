package uz.ssd.testapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ssd.testapp.entity.Variable;

public interface VariableRepository extends JpaRepository<Variable, Long> {
}
