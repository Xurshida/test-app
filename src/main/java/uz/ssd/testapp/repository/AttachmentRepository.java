package uz.ssd.testapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ssd.testapp.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {
}
