package uz.ssd.testapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ssd.testapp.entity.Test;
import uz.ssd.testapp.projection.TestColl;

import java.util.List;

public interface TestRepository extends JpaRepository<Test, Long> {
    List<TestColl> findAllByDelete(boolean delete);
}
