package uz.ssd.testapp.model;

import lombok.Data;

@Data
public class MailResponse {
    private String name;
    private String user;
    private String ip;
    private String pass;
}
