package uz.ssd.testapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Status {
    private int code;
    private Object message;

    public Status(int code) {
        this.code = code;
        this.message = code != 200 ? code != 500 ? code != 404 ? code != 100 ? code != 400 ? "????" : "You are sending the wrong parameter!" : "Unspecified error" : "Not found" : "Innalillahi va inna ilayhi rojiun" : "Alhamdulillah";
    }
}
