package uz.ssd.testapp.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResAnswer {
    private Long id;
    private Long questionId;
    private Long variantId;
    private boolean correct;
}
