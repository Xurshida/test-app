package uz.ssd.testapp.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqVariable {
    private Long id;
    private int questionOneNumber;
    private int timeOneNumber;
    private int questionTwoNumber;
    private int timeTwoNumber;
    private int acceptableOneNumber;
    private int acceptableTwoNumber;
    private int maxLoginAttempts;
}
