package uz.ssd.testapp.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqTest {
    private Long id;
    private short category; //1 yoki 2
    private String title;
    private String description;
}
