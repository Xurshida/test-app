package uz.ssd.testapp.payload;

import lombok.Data;

@Data
public class ReqAnswer {
    private Long questionId;
    private Long variantId;
}
