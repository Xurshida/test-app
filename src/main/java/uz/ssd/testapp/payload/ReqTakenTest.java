package uz.ssd.testapp.payload;

import lombok.Data;

import java.util.List;

@Data
public class ReqTakenTest {
    private Long id;
    private Long testId;
}
