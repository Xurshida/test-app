package uz.ssd.testapp.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.ssd.testapp.entity.Attachment;
import uz.ssd.testapp.entity.Variant;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqQuestion {
    private Long id;
    private String title;
    private String description;
    private Long photo;
    private List<Variant> variant;
}
