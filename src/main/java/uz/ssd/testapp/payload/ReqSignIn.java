package uz.ssd.testapp.payload;

import lombok.Data;

@Data
public class ReqSignIn {
    private String email;
    private String password;
}
