package uz.ssd.testapp.payload;

import lombok.Data;

@Data
public class ReqUser {
    private Long avatarId;
    private String lastname;
    private String firstname;
    private String middlename;
    private String email;
    private String gender;
    private String newPassword;
}
