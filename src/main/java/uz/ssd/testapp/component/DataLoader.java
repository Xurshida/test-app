package uz.ssd.testapp.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.ssd.testapp.entity.User;
import uz.ssd.testapp.entity.enums.RoleName;
import uz.ssd.testapp.repository.RoleRepository;
import uz.ssd.testapp.repository.UserRepository;

import java.util.Arrays;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(new User(
                    "123",
                    passwordEncoder.encode("123"),
                    "John",
                    "Doe",
                    roleRepository.findAllByName(RoleName.ROLE_SUPER_ADMIN)
            ));
        }
    }
}
