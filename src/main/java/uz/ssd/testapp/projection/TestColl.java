package uz.ssd.testapp.projection;

public interface TestColl {
    Long getId();
    String getTitle();
    String getDescription();
}
