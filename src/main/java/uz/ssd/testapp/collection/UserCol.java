package uz.ssd.testapp.collection;

public interface UserCol {
    Long getId();
    String getLastname();
    String getFirstname();
    String getMiddlename();
    Long getAvatarId();
    String getPassword();
    boolean isDeleted();
}
