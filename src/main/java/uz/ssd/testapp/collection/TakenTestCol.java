package uz.ssd.testapp.collection;

import uz.ssd.testapp.entity.Answer;

import java.util.List;

public interface TakenTestCol {
    Long getId();
    Double getTotalScore();
    UserColl getUser();
    TestCol getTest();
    List<AnswerCol> getAnswersOne();
    List<AnswerCol> getAnswersTwo();
}
