package uz.ssd.testapp.collection;

public interface AnswerCol {
    Long getId();
    QuestionCol getQuestion();
    VariantCol getVariant();
    boolean isCorrect();
    double getObtainedScore();
}
