package uz.ssd.testapp.security;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.ssd.testapp.entity.Role;
import uz.ssd.testapp.entity.User;
import uz.ssd.testapp.entity.enums.RoleName;
import uz.ssd.testapp.exception.ResourceNotFoundException;
import uz.ssd.testapp.model.MailRequest;
import uz.ssd.testapp.model.MailResponse;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.JwtResponse;
import uz.ssd.testapp.payload.ReqSignUp;
import uz.ssd.testapp.repository.RoleRepository;
import uz.ssd.testapp.repository.UserRepository;
import uz.ssd.testapp.repository.VariableRepository;
import uz.ssd.testapp.service.MailService;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class AuthService implements UserDetailsService {
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthenticationManager authenticate;

    @Autowired
    MailService mailService;

    @Autowired
    VariableRepository variableRepository;

    @Value("${spring.mail.password}")
    private String mailPassword;

    @Value("${spring.mail.username}")
    private String mailUsername;

    public AuthService(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.getByEmail(email).orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public UserDetails loadUserById(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User id not found: " + userId));
    }

    public ApiResponseModel register(ReqSignUp reqSignUp, String roleName) throws Exception {
        ApiResponseModel response = new ApiResponseModel();
        Optional<User> optionalUser = userRepository.getByPhoneNumber(reqSignUp.getPhoneNumber());
        Optional<User> optionalUser1 = userRepository.getByEmail(reqSignUp.getEmail());
        Optional<User> optionalUser2 = userRepository.getByPassportNumber(reqSignUp.getPassportNumber());
        if (optionalUser.isPresent()) {
            return new ApiResponseModel(HttpStatus.CONFLICT.value(), "phone number exists", null);
        } else if (optionalUser1.isPresent()) {
            return new ApiResponseModel(HttpStatus.CONFLICT.value(), "email exists", null);
        } else if (optionalUser2.isPresent()) {
            return new ApiResponseModel(HttpStatus.CONFLICT.value(), "passport number exists", null);
        } else {
            User user = new User();
            if (reqSignUp.getId() != null) {
                user = userRepository.findById(reqSignUp.getId()).orElseThrow(() -> new ResourceNotFoundException("user", "id", reqSignUp.getId()));
            }
            if (roleName.equals(RoleName.ROLE_USER.name().toLowerCase())) {
                user.setRoles(roleRepository.findAllByNameIn(
                        Arrays.asList(RoleName.ROLE_USER)
                ));
                response.setMessage("user created");
            } else if (roleName.equals(RoleName.ROLE_ADMIN.name().toLowerCase())) {
                user.setRoles(roleRepository.findAllByNameIn(
                        Arrays.asList(RoleName.ROLE_ADMIN)
                ));
                response.setMessage("admin created");
            } else if (roleName.equals(RoleName.ROLE_SUPER_ADMIN.name().toLowerCase())) {
                if (userRepository.countAllByRoles(roleRepository.findByName(RoleName.ROLE_SUPER_ADMIN).get().getId()) < 2){
                    user.setRoles(roleRepository.findAllByNameIn(
                            Arrays.asList(RoleName.ROLE_SUPER_ADMIN)
                    ));
                    response.setMessage("superadmin created");
                }else {
                    return new ApiResponseModel(HttpStatus.CONFLICT.value(), "There are already 2 superadmins in the system.You cannot add more!", null);
                }
            }
            String password = generatePassword();
            MailResponse mailResponse = new MailResponse();
            mailResponse.setName(reqSignUp.getFirstname() + " " + reqSignUp.getLastname());
            mailResponse.setIp("localhost:8080/api/auth/register");
            mailResponse.setUser(reqSignUp.getEmail());
            mailResponse.setPass(password);
            mailService.sendEmail(new MailRequest("Info", reqSignUp.getEmail(), mailUsername, ""), mailResponse);
            user.setEmail(reqSignUp.getEmail());
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(reqSignUp.getDateOfBirth());
            user.setDateOfBirth(date);
            user.setPassword(passwordEncoder.encode(password));
            user.setPhoneNumber(reqSignUp.getPhoneNumber());
            user.setPassportNumber(reqSignUp.getPassportNumber());
            user.setLastname(reqSignUp.getFirstname());
            user.setFirstname(reqSignUp.getLastname());
            user.setEnabled(true);
            userRepository.save(user);
            response.setStatusCode(HttpStatus.CREATED.value());
            response.setData(user);
            return response;
        }
    }

    public String generatePassword() {
        int length = 8;
        boolean useLetters = true;
        boolean useNumbers = true;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    public HttpEntity<?> getApiToken(String email, String password) {
        Authentication authentication = authenticate.authenticate(
                new UsernamePasswordAuthenticationToken(email, password)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);
        User user = userRepository.getByEmail(email).orElseThrow(() -> new ResourceNotFoundException("user", "email", email));
        if (user.getRoles().contains(roleRepository.findByName(RoleName.ROLE_USER).get())) {
            int limit = variableRepository.getOne(1L).getMaxLoginAttempts();
            if (user.getLoginAttempts() == limit) {
                Role roleBlocked = roleRepository.findByName(RoleName.ROLE_BLOCKED).orElseThrow(() -> new ResourceNotFoundException("role", "rolename", RoleName.ROLE_BLOCKED));
                List<Role> roles = new ArrayList<>(user.getRoles());
                roles.add(roleBlocked);
                user.setRoles(roles);
                user.setLoginAttempts((user.getLoginAttempts() + 1));
                userRepository.save(user);
            }else if (user.getLoginAttempts() > limit) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponseModel(HttpStatus.CONFLICT.value(), "Login attempts exceeded limit of " + limit));
            } else {
                user.setLoginAttempts((user.getLoginAttempts() + 1));
                userRepository.save(user);
            }
        }
        return ResponseEntity.ok(new JwtResponse(jwt));
    }
}
