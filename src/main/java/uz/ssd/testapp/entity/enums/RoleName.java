package uz.ssd.testapp.entity.enums;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_SUPER_ADMIN,
    ROLE_BLOCKED
}
