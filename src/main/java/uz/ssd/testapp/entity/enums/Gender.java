package uz.ssd.testapp.entity.enums;

public enum Gender {
    MALE,
    FEMALE
}
