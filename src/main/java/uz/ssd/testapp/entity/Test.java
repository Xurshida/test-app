package uz.ssd.testapp.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.ssd.testapp.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Test extends AbsEntity {
    @NotNull
    private short category;

    @Column(columnDefinition = "text")
    private String title;

    @Column(columnDefinition = "text")
    private String description;

    private boolean delete;

    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Question> questionOne;

    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Question> questionTwo;

    @ManyToMany
    private List<User> accessUser;

    public Test(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
