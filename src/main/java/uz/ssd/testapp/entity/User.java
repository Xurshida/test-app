package uz.ssd.testapp.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.ssd.testapp.entity.enums.Gender;
import uz.ssd.testapp.entity.template.AbsEntity;

import javax.annotation.MatchesPattern;
import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbsEntity implements UserDetails {

    private String firstname;
    private String lastname;
    private String middlename;

    @Column(unique = true)
    @NotNull
    @MatchesPattern("[A-Z]{2}[0-9]{7}")
    private String passportNumber;

    private Date dateOfBirth;

    @Column(unique = true)
    @MatchesPattern("(?:\\+\\([9]{2}[8]\\)[0-9]{2}\\ [0-9]{3}\\-[0-9]{2}\\-[0-9]{2}") //+998931234988
    private String phoneNumber;

    @Column(unique = true)
    @NotNull
    @MatchesPattern("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    private String email;

    @NotNull
    private String password;

    @OneToOne(fetch = FetchType.LAZY)
    private Attachment avatar;

    @ManyToMany
    private List<Test> test;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private int loginAttempts = 0;

    private boolean deleted = false;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles;

    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;

    public User(String s, String s1, String encode, String firstname, String lastname, List<Role> allByNameIn) {
    }


    public User(String email, String password, String firstname, String lastname, List<Role> roles) {
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.roles = roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

}
