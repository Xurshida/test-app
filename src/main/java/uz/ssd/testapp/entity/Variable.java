package uz.ssd.testapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.ssd.testapp.entity.template.AbsEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Variable extends AbsEntity {
    private int questionOneNumber;
    private int timeOneNumber;
    private int questionTwoNumber;
    private int timeTwoNumber;
    private int acceptableOneNumber;
    private int acceptableTwoNumber;
    private int maxLoginAttempts;
}
