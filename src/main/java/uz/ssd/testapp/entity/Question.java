package uz.ssd.testapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.ssd.testapp.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Question extends AbsEntity {

    @Column(columnDefinition = "text")
    private String title;

    @Column(columnDefinition = "text")
    private String description;

    private boolean delete;

    @ManyToOne
    private Attachment photo;

    @OneToMany(cascade = CascadeType.REMOVE)
    private List<Variant> variant;

    public Question(String title, String description, List<Variant> variant) {
        this.title = title;
        this.description = description;
        this.variant = variant;
    }

}
