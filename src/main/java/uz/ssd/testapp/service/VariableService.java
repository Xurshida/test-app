package uz.ssd.testapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.ssd.testapp.entity.Variable;
import uz.ssd.testapp.exception.ResourceNotFoundException;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.ReqVariable;
import uz.ssd.testapp.repository.VariableRepository;

@Service
public class VariableService {
    @Autowired
    VariableRepository variableRepository;

    public ApiResponseModel changeVariable(ReqVariable reqVariable){
        try {
            Variable variable = variableRepository.findById(reqVariable.getId()).orElseThrow(() -> new ResourceNotFoundException("variable", "id", reqVariable.getId()));
            variable.setId(reqVariable.getId());
            variable.setAcceptableOneNumber(reqVariable.getAcceptableOneNumber());
            variable.setAcceptableTwoNumber(reqVariable.getAcceptableTwoNumber());
            variable.setMaxLoginAttempts(reqVariable.getMaxLoginAttempts());
            variable.setQuestionOneNumber(reqVariable.getQuestionOneNumber());
            variable.setQuestionTwoNumber(reqVariable.getQuestionTwoNumber());
            variable.setTimeOneNumber(reqVariable.getTimeOneNumber());
            variable.setTimeTwoNumber(reqVariable.getTimeTwoNumber());
            variableRepository.save(variable);
            return new ApiResponseModel(HttpStatus.OK.value(), "Edited successfully");
        }catch (Exception e){
            return new ApiResponseModel(e.hashCode(), e.getMessage());
        }

    }

    public ApiResponseModel getVariable(){
        ApiResponseModel response = new ApiResponseModel();
        try {
            Variable variable = variableRepository.findById(1L).orElseThrow(() -> new ResourceNotFoundException("variable", "id", 1L));
            response.setStatusCode(HttpStatus.FOUND.value());
            response.setMessage("Found");
            ReqVariable resVariant = new ReqVariable(
                    variable.getId(),
                    variable.getQuestionOneNumber(),
                    variable.getQuestionTwoNumber(),
                    variable.getAcceptableOneNumber(),
                    variable.getAcceptableTwoNumber(),
                    variable.getTimeOneNumber(),
                    variable.getTimeTwoNumber(),
                    variable.getMaxLoginAttempts());
            response.setData(resVariant);
        }catch (Exception e){
            response.setStatusCode(e.hashCode());
            response.setMessage(e.getMessage());
        }
        return response;
    }
}
