package uz.ssd.testapp.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import uz.ssd.testapp.model.MailRequest;
import uz.ssd.testapp.model.MailResponse;
import uz.ssd.testapp.payload.ApiResponseModel;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class MailService {
    @Autowired
    private JavaMailSender sender;

    @Autowired
    private Configuration config;

    public ApiResponseModel sendEmail(MailRequest request, MailResponse mailResponse) throws Exception{
        MimeMessage message = sender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
//            helper.addAttachment("logo.png", new ClassPathResource("logo.png"));
            Template t = config.getTemplate("email.ftl");

            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mailResponse);
            helper.setTo(request.getTo());
            helper.setText(html, true);
            helper.setSubject(request.getSubject());
            helper.setFrom(request.getFrom());
            sender.send(message);
            return new ApiResponseModel(HttpStatus.OK.value(), "mail send to : " + request.getTo());
        } catch (MessagingException | IOException | TemplateException e) {
            return new ApiResponseModel(HttpStatus.OK.value(),"Mail Sending failure : " + e.getMessage());
        }
    }
}
