package uz.ssd.testapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.ssd.testapp.entity.Attachment;
import uz.ssd.testapp.entity.AttachmentContent;
import uz.ssd.testapp.exception.ResourceNotFoundException;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.ResUploadFile;
import uz.ssd.testapp.repository.AttachmentContentRepository;
import uz.ssd.testapp.repository.AttachmentRepository;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @Transactional
    public ApiResponseModel uploadFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> iterator = request.getFileNames();
        MultipartFile file;
        List<ResUploadFile> resUploadFiles = new ArrayList<>();
        while (iterator.hasNext()) {
            file = request.getFile(iterator.next());
            if (file.getSize() > 2048000) {
                return new ApiResponseModel(HttpStatus.CONFLICT.value(), "Rasmning hajmi 2 MB dan ortiq.", resUploadFiles);
            }else{
                AttachmentContent attachmentContent = attachmentContentRepository.save(new AttachmentContent(file.getBytes()));
                Attachment attachment = attachmentRepository.save(new Attachment(
                        file.getOriginalFilename(),
                        file.getContentType(),
                        file.getSize(),
                        attachmentContent));
                resUploadFiles.add(new ResUploadFile(attachment.getId(),
                        attachment.getName(),
                        ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/attach/").path(attachment.getId().toString()).toUriString(),
                        attachment.getContentType(),
                        attachment.getSize()));
                return new ApiResponseModel(HttpStatus.CREATED.value(), "Saved", resUploadFiles);
            }
        }
        return new ApiResponseModel(HttpStatus.CONFLICT.value(), "Not saved");
    }

    public HttpEntity<?> getAttachmentContent(Long attachmentId, HttpServletResponse response) {
        Attachment attachment = attachmentRepository.findById(attachmentId).orElseThrow(() -> new ResourceNotFoundException("Attachment", "id", attachmentId));

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attachment.getName() + "\"")
                .body(attachment.getAttachmentContent().getContent());
    }

    @Transactional
    public ApiResponseModel deleteAttachment(Long attachmentId) {
        if (attachmentRepository.existsById(attachmentId)) {
            attachmentRepository.deleteById(attachmentId);
            return new ApiResponseModel(HttpStatus.OK.value(), "Attachment deleted");
        }
        return new ApiResponseModel(HttpStatus.CONFLICT.value(), "Attachment not deleted");
    }
}
