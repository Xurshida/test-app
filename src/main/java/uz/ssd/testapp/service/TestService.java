package uz.ssd.testapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.ssd.testapp.entity.*;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.ReqQuestion;
import uz.ssd.testapp.payload.ReqTest;
import uz.ssd.testapp.repository.QuestionRepository;
import uz.ssd.testapp.repository.TestRepository;
import uz.ssd.testapp.repository.UserRepository;
import uz.ssd.testapp.repository.VariantRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TestService {

    @Autowired
    TestRepository testRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    VariantRepository variantRepository;

    public ApiResponseModel saveOrEdit(ReqTest reqTest) {
        try {
            if (reqTest.getId()!=null) {
                Optional<Test> testOptional = testRepository.findById(reqTest.getId());
                if (testOptional.isPresent()) {
                    Test test = testOptional.get();
                    test.setCategory(reqTest.getCategory());
                    test.setDescription(reqTest.getDescription());
                    test.setTitle(reqTest.getTitle());
                    testRepository.save(test);
                    return new ApiResponseModel(202, "edit");
                }
                return new ApiResponseModel(404,"no information was found on this id= "+reqTest.getId());
            }
            Test test=new Test();
            test.setDescription(reqTest.getDescription());
            test.setTitle(reqTest.getTitle());
            testRepository.save(test);
            return new ApiResponseModel(201,"create");
        }catch (Exception e){
            return new ApiResponseModel(404,"error");
        }
    }

    public ApiResponseModel deleteTest(Long id) {
        try {
                Test test=testRepository.getOne(id);
                test.setDelete(true);
                testRepository.save(test);
                return new ApiResponseModel(202, "delete");
        }catch (Exception e){
            return new ApiResponseModel(404,"not delete please try again");
        }
    }

    private List<ReqTest> generationTest(List<Test> testList) {
        List<ReqTest> tests=new ArrayList<>();
        for (Test test : testList) {
            tests.add(new ReqTest(test.getId(), test.getCategory(), test.getTitle(),test.getDescription()));
        }
        return tests;
    }

    public ApiResponseModel getTests(User user) {
        try {
            return new ApiResponseModel(302,"found",generationTest(user.getTest()));
        }catch (Exception e){
            return new ApiResponseModel(404,"not found");
        }
    }

    public ApiResponseModel getTestsQuestionOne(Long id, User user) {
        try {
            for (Test test : user.getTest()) {
                if (test.getId().equals(id)){
                    return new ApiResponseModel(404,"found",test.getQuestionOne());
                }
            }
            return new ApiResponseModel(404,"you are not allowed");
        }catch (Exception e){
            return new ApiResponseModel(404,"not found");
        }
    }

    public ApiResponseModel saveQuestionOne(List<ReqQuestion> reqQuestions, Long id) {
        try {
            try {
                Test test=testRepository.getOne(id);
                List<Question> questions = test.getQuestionOne();
                for (ReqQuestion question : reqQuestions) {
                    Question saveQuestion=new Question();
                    saveQuestion.setTitle(question.getTitle());
                    saveQuestion.setDescription(question.getDescription());
                    List<Variant> variants=new ArrayList<>();
                    for (Variant variant : question.getVariant()) {
                        variants.add(variantRepository.save(variant));
                    }
                    saveQuestion.setVariant(variants);
                    questions.add(questionRepository.save(saveQuestion));
                }
                testRepository.save(test);
                return new ApiResponseModel(202, "save");
            }catch (Exception e){
                return new ApiResponseModel(404,"no information was found on this id= "+id);
            }
        }catch (Exception e){
            return new ApiResponseModel(404,"not found");
        }
    }

    public ApiResponseModel permission(Long testId, Long userId) {
        try {
            Test testO;
            User user;
            try {
               user=userRepository.getOne(userId);
            }catch (Exception e){
                return new ApiResponseModel(404,"this user not found");
            }
            try {
                testO=testRepository.getOne(testId);
            }catch (Exception e){
                return new ApiResponseModel(404,"this test not found");
            }
            List<Test> test = user.getTest();
            test.add(testO);
            user.setTest(test);
            userRepository.save(user);
            return new ApiResponseModel(HttpStatus.OK.value(),"OK");
        }catch (Exception e){
            return new ApiResponseModel(HttpStatus.CONFLICT.value(),"CONFLICT");
        }
    }

    public ApiResponseModel unPermission(Long testId, Long userId) {
        try {
            Test testO;
            User user;
            try {
                user=userRepository.getOne(userId);
            }catch (Exception e){
                return new ApiResponseModel(404,"this user not found");
            }
            try {
                testO=testRepository.getOne(testId);
            }catch (Exception e){
                return new ApiResponseModel(404,"this test not found");
            }
            List<Test> test = user.getTest();
            test.remove(testO);
            user.setTest(test);
            userRepository.save(user);
            return new ApiResponseModel(HttpStatus.OK.value(),"OK");
        }catch (Exception e){
            return new ApiResponseModel(HttpStatus.CONFLICT.value(),"CONFLICT");
        }
    }

    public ApiResponseModel saveQuestionTwo(List<ReqQuestion> reqQuestions, Long id) {
        try {
            try {
                Test test=testRepository.getOne(id);
                List<Question> questions = test.getQuestionTwo();
                reqQuestions.forEach(question->{
                    List<Variant> variants=new ArrayList<>();
                    question.getVariant().forEach(variant->variants.add(variantRepository.save(variant)));
                    questions.add(questionRepository.save(new Question(question.getTitle(), question.getDescription(), variants)));
                });
                testRepository.save(test);
                return new ApiResponseModel(202, "save");
            }catch (Exception e){
                return new ApiResponseModel(404,"no information was found on this id= "+id);
            }
        }catch (Exception e){
            return new ApiResponseModel(404,"not found");
        }
    }

    public ApiResponseModel getTestsQuestionTwo(Long id, User user) {
        try {
            for (Test test : user.getTest()) {
                if (test.getId().equals(id)){
                    return new ApiResponseModel(404,"found",test.getQuestionTwo());
                }
            }
            return new ApiResponseModel(404,"you are not allowed");
        }catch (Exception e){
            return new ApiResponseModel(404,"not found");
        }
    }

    public ApiResponseModel changeQuestion(ReqQuestion reqQuestion) {
        try {
                Question questionSave=questionRepository.getOne(reqQuestion.getId());
                List<Variant> variant = questionSave.getVariant();
                List<Variant> variantSave = new ArrayList<>();
                for (Variant variant1 : reqQuestion.getVariant()) {
                    variantSave.add(variantRepository.save(new Variant(variant1.getName(), variant1.getText(), variant1.isCorrect())));
                }
                questionSave.setVariant(variantSave);
                questionSave.setTitle(reqQuestion.getTitle());
                questionSave.setDescription(reqQuestion.getDescription());
                questionRepository.save(questionSave);
                for (Variant variant1 : variant) {
                    variantRepository.delete(variant1);
                }
                return new ApiResponseModel(202, "edit");
        }catch (Exception e){
            return new ApiResponseModel(404,"not edit please try again");
        }
    }
}
