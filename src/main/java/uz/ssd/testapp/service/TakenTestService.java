package uz.ssd.testapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.ssd.testapp.collection.TakenTestCol;
import uz.ssd.testapp.entity.*;
import uz.ssd.testapp.exception.ResourceNotFoundException;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.ReqAnswer;
import uz.ssd.testapp.payload.ReqTakenTest;
import uz.ssd.testapp.repository.*;
import uz.ssd.testapp.utils.CommonUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TakenTestService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    TestRepository testRepository;

    @Autowired
    AnswerService answerService;

    @Autowired
    TakenTestRepository takenTestRepository;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    VariableRepository variableRepository;

    public ApiResponseModel saveOrEdit(ReqTakenTest reqTakenTest, User user){
        ApiResponseModel response = new ApiResponseModel();
        try {
            TakenTest takenTest = new TakenTest();
            response.setMessage("Created");
            if (reqTakenTest.getId() != null){
                takenTest.setId(reqTakenTest.getId());
                response.setMessage("Edited");
            }
            takenTest.setUser(user);
            takenTest.setTest(testRepository.findById(reqTakenTest.getTestId()).orElseThrow(
                    () -> new ResourceNotFoundException("test", "id", reqTakenTest.getTestId())));

            takenTestRepository.save(takenTest);
            response.setStatusCode(HttpStatus.OK.value());
            response.setData(takenTest);
        }catch (Exception e){
            response.setStatusCode(e.hashCode());
            response.setMessage(e.getMessage());
        }
        return response;
    }

    public ApiResponseModel saveAnswersOne(List<ReqAnswer> answersOne, Long takenTestId){
        ApiResponseModel response = new ApiResponseModel();
        try {
            TakenTest takenTest = takenTestRepository.findById(takenTestId).orElseThrow(
                    () -> new ResourceNotFoundException("test", "id", takenTestId));
            List<Answer> answers = answersOne.stream().map(
                    reqAnswer -> answerService.saveAnswer(reqAnswer)).collect(Collectors.toList());
            takenTest.setAnswersOne(answers);
            double totalScore = 0;
            for (Answer answer : answers) {
                totalScore += answer.getObtainedScore();
            }
            takenTest.setTotalScoreOne(totalScore);
            if (totalScore >= variableRepository.getOne(1L).getAcceptableOneNumber()){
                takenTest.setAcceptedFromFirst(true);
            }
            takenTestRepository.save(takenTest);
            response.setStatusCode(HttpStatus.OK.value());
            response.setData(takenTest);
        }catch (Exception e){
            response.setStatusCode(e.hashCode());
            response.setMessage(e.getMessage());
        }
        return response;
    }

    public ApiResponseModel saveAnswersTwo(List<ReqAnswer> answersTwo, Long takenTestId){
        ApiResponseModel response = new ApiResponseModel();
        try {
            TakenTest takenTest = takenTestRepository.findById(takenTestId).orElseThrow(
                    () -> new ResourceNotFoundException("test", "id", takenTestId));
            List<Answer> answers = answersTwo.stream().map(
                    reqAnswer -> answerService.saveAnswer(reqAnswer)).collect(Collectors.toList());
            takenTest.setAnswersTwo(answers);
            double totalScore = 0;
            for (Answer answer : answers) {
                totalScore += answer.getObtainedScore();
            }
            takenTest.setTotalScoreTwo(totalScore);
            if (totalScore >= variableRepository.getOne(1L).getAcceptableTwoNumber()){
                takenTest.setAcceptedFromSecond(true);
            }
            takenTestRepository.save(takenTest);
            response.setStatusCode(HttpStatus.OK.value());
            response.setData(takenTest);
        }catch (Exception e){
            response.setStatusCode(e.hashCode());
            response.setMessage(e.getMessage());
        }
        return response;
    }

    public ApiResponseModel getTakenTest(Long id){
        ApiResponseModel response = new ApiResponseModel();
        try {
            TakenTestCol takenTest = takenTestRepository.getById(id).orElseThrow(() -> new ResourceNotFoundException("TakenTest", "id", id));
            response.setStatusCode(HttpStatus.FOUND.value());
            response.setMessage("Found");
            response.setData(takenTest);
        }catch (Exception e){
            response.setStatusCode(e.hashCode());
            response.setMessage(e.getMessage());
        }
        return response;
    }

    public ApiResponseModel getTakenTests(Integer page, Integer size){
        ApiResponseModel response = new ApiResponseModel();
        try {
            Page<TakenTestCol> takenTests = takenTestRepository.findAllBy(CommonUtils.getPageableById(page, size));
            response.setMessage("Found");
            if (takenTests.isEmpty()){
                response.setMessage("TakenTest table is empty");
            }
            response.setStatusCode(HttpStatus.FOUND.value());
            response.setData(takenTests);
        }catch (Exception e){
            response.setStatusCode(e.hashCode());
            response.setMessage(e.getMessage());
        }
        return response;
    }


}
