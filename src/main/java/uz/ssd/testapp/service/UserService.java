package uz.ssd.testapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.ssd.testapp.collection.UserCol;
import uz.ssd.testapp.entity.Role;
import uz.ssd.testapp.entity.User;
import uz.ssd.testapp.entity.enums.Gender;
import uz.ssd.testapp.entity.enums.RoleName;
import uz.ssd.testapp.exception.ResourceNotFoundException;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.ReqSignUp;
import uz.ssd.testapp.payload.ReqUser;
import uz.ssd.testapp.repository.AttachmentRepository;
import uz.ssd.testapp.repository.RoleRepository;
import uz.ssd.testapp.repository.UserRepository;
import uz.ssd.testapp.security.AuthService;
import uz.ssd.testapp.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AuthService authService;

    private final PasswordEncoder passwordEncoder;

    public UserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public ApiResponseModel getUser(Long id) {
        ApiResponseModel responseModel = new ApiResponseModel();
        try {
            User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("user", "id", id));
            responseModel.setStatusCode(HttpStatus.OK.value());
            responseModel.setMessage("Found");
            responseModel.setData(user);
        } catch (Exception e) {
            responseModel.setStatusCode(e.hashCode());
            responseModel.setMessage(e.getMessage());
        }
        return responseModel;
    }

    public ApiResponseModel blockOrUnblock(Long id) {
        ApiResponseModel responseModel = new ApiResponseModel();
        try {
            User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("user", "id", id));
            Optional<Role> roleBlocked = roleRepository.findByName(RoleName.ROLE_BLOCKED);
            if (user.getRoles().contains(roleBlocked.get())){
                user.setEnabled(true);
                List<Role> roles = new ArrayList<>(user.getRoles());
                roles.remove(roleBlocked.get());
                user.setRoles(roles);
                userRepository.save(user);
                return new ApiResponseModel(HttpStatus.OK.value(), "User is unblocked",  user);

            }else {
                user.setEnabled(false);
                List<Role> roles = new ArrayList<>(user.getRoles());
                roles.add(roleBlocked.get());
                user.setRoles(roles);
                userRepository.save(user);
                return new ApiResponseModel(HttpStatus.OK.value(), "User is unblocked",  user);

            }
        } catch (Exception e) {
            responseModel.setStatusCode(e.hashCode());
            responseModel.setMessage(e.getMessage());
        }

        return responseModel;
    }

    public ApiResponseModel changeProfileSettings(ReqUser reqUser, User user) {
        ApiResponseModel responseModel = new ApiResponseModel();
        try {
            user.setAvatar(attachmentRepository.getOne(reqUser.getAvatarId()));
            user.setLastname(reqUser.getLastname());
            user.setFirstname(reqUser.getFirstname());
            user.setMiddlename(reqUser.getMiddlename());
            user.setEmail(reqUser.getEmail());
            user.setGender(Gender.valueOf(reqUser.getGender()));
            user.setPassword(passwordEncoder.encode(reqUser.getNewPassword()));
            userRepository.save(user);
            responseModel.setStatusCode(HttpStatus.OK.value());
            responseModel.setMessage(HttpStatus.OK.getReasonPhrase());
            responseModel.setData(user);
        } catch (Exception e) {
            responseModel.setStatusCode(e.hashCode());
            responseModel.setMessage(e.getMessage());
        }

        return responseModel;
    }

    public ApiResponseModel deleteAdmin(Long id) {
        Optional<User> admin = userRepository.findById(id);
        Role role = roleRepository.findByName(RoleName.ROLE_ADMIN).orElseThrow(() -> new ResourceNotFoundException("role", "name", RoleName.ROLE_ADMIN));
        admin.ifPresent(user -> user.getRoles().remove(role));
        return new ApiResponseModel(HttpStatus.OK.value(), "Admin is deleted");
    }

    public ApiResponseModel blockOrUnblockAdmin(Long id) {
        Optional<User> user = userRepository.findById(id);
        Optional<Role> roleAdmin = roleRepository.findByName(RoleName.ROLE_ADMIN);
        Optional<Role> roleBlocked = roleRepository.findByName(RoleName.ROLE_BLOCKED);
        if (user.isPresent() && roleAdmin.isPresent() && roleBlocked.isPresent()) {
            if (user.get().getRoles().contains(roleAdmin.get()) && user.get().getRoles().contains(roleBlocked.get())) {
                List<Role> roles = new ArrayList<>(user.get().getRoles());
                roles.remove(roleBlocked.get());
                user.get().setRoles(roles);
                userRepository.save(user.get());
                return new ApiResponseModel(HttpStatus.OK.value(), "Admin is unblocked",  user.get());
            } else if (user.get().getRoles().contains(roleAdmin.get()) && !user.get().getRoles().contains(roleBlocked.get())) {
                List<Role> roles = new ArrayList<>(user.get().getRoles());
                roles.add(roleBlocked.get());
                user.get().setRoles(roles);
                userRepository.save(user.get());
                return new ApiResponseModel(HttpStatus.OK.value(), "Admin is blocked", user.get());
            }
            return new ApiResponseModel(HttpStatus.CONFLICT.value(), "User is not admin", user.get());
        }
        return new ApiResponseModel(HttpStatus.NOT_FOUND.value(), "User not found");
    }

    public ApiResponseModel getAdmin(Long id) {
        User admin = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("user", "id", id));
        return new ApiResponseModel(HttpStatus.FOUND.value(), "Found", admin);
    }

    public ApiResponseModel getAllUsers(String startTime, String endTime, RoleName roleName, Integer page, Integer size, String search) {
        ApiResponseModel response = new ApiResponseModel();
        try {
            Optional<Role> role = roleRepository.findByName(roleName);
            if (role.isPresent()) {
                Integer roleId = role.get().getId();
                Page<UserCol> users = userRepository.findAllByCreatedAtBetweenAndRolesQuery(
                        startTime,
                        endTime,
                        roleId,
                        CommonUtils.getPageableById(page, size));
                if (!search.equals("all")) {
                    users = userRepository.findAllByCreatedAtBetweenAndRolesContainsQuery(
                            startTime,
                            endTime,
                            roleId,
                            search,
                            CommonUtils.getPageableById(page, size));
                }
                response.setMessage("Found");
                response.setStatusCode(HttpStatus.OK.value());
                response.setData(users);
            }
        } catch (Exception e) {
            response.setMessage("Not found");
            response.setStatusCode(HttpStatus.CONFLICT.value());
        }
        return response;
    }
}
