package uz.ssd.testapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.ssd.testapp.collection.AnswerCol;
import uz.ssd.testapp.entity.Answer;
import uz.ssd.testapp.entity.Question;
import uz.ssd.testapp.entity.Variant;
import uz.ssd.testapp.exception.ResourceNotFoundException;
import uz.ssd.testapp.payload.ApiResponseModel;
import uz.ssd.testapp.payload.ReqAnswer;
import uz.ssd.testapp.payload.ResAnswer;
import uz.ssd.testapp.repository.AnswerRepository;
import uz.ssd.testapp.repository.QuestionRepository;
import uz.ssd.testapp.repository.VariantRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnswerService {
    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    VariantRepository variantRepository;

    public Answer saveAnswer(ReqAnswer reqAnswer){
        Answer answer = new Answer();
        Question question = questionRepository.findById(reqAnswer.getQuestionId()).orElseThrow(
                () -> new ResourceNotFoundException("question", "id", reqAnswer.getQuestionId()));
        answer.setQuestion(question);
        answer.setVariant(variantRepository.findById(reqAnswer.getVariantId()).orElseThrow(
                () -> new ResourceNotFoundException("variant", "id", reqAnswer.getVariantId())));
        List<Variant> variants = question.getVariant();
        for (Variant variant : variants) {
            if (variant.isCorrect()){
                if (reqAnswer.getVariantId().equals(variant.getId())){
                    answer.setCorrect(true);
                    answer.setObtainedScore(1);
                }
            }
        }
        answerRepository.save(answer);
        return answer;
    }

    public ApiResponseModel getAnswer(Long id){
        ApiResponseModel response = new ApiResponseModel();
        try {
            Answer answer = answerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("answer", "id", id));
            response.setStatusCode(HttpStatus.FOUND.value());
            response.setMessage("Found");
            ResAnswer resAnswer = new ResAnswer(
                    answer.getId(),
                    answer.getQuestion().getId(),
                    answer.getVariant().getId(),
                    answer.isCorrect());
            response.setData(resAnswer);
        }catch (Exception e){
            response.setStatusCode(e.hashCode());
            response.setMessage(e.getMessage());
        }
        return response;
    }

    public ApiResponseModel getAnswers(Long takenTestId){
        ApiResponseModel response = new ApiResponseModel();
        try {
            List<Answer> answers = answerRepository.findAll();
            response.setMessage("Found");
            if (answers.isEmpty()){
                response.setMessage("Answer table is empty");
            }
            List<ResAnswer> resAnswers = new ArrayList<>();
            for (Answer answer : answers) {
                ResAnswer resAnswer = new ResAnswer(
                        answer.getId(),
                        answer.getQuestion().getId(),
                        answer.getVariant().getId(),
                        answer.isCorrect());
                resAnswers.add(resAnswer);
            }
            response.setStatusCode(HttpStatus.FOUND.value());
            response.setData(resAnswers);
        }catch (Exception e){
            response.setStatusCode(e.hashCode());
            response.setMessage(e.getMessage());
        }
        return response;
    }

}
