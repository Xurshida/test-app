<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>

<body style="margin: 0; padding: 0;">
    <table style="width: 600px; border: 1px solid black; text-align: center;">
        <thead>
            <tr>
                <th style="padding: 0 10px;"><h3>THE CAPITAL MARKETS DEVELOPMENT AGENCY REPUBLIC OF UZBEKISTAN</h3></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><p>Уважаемый ${name}</p></td>
                <td><p>Логин: ${user}</p> <p>Парол: ${pass}</p></td>
            </tr>
            <tr>
                <td>
                    <p>Линк для входа => <a href="http://${ip}">${ip}</a></p>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>