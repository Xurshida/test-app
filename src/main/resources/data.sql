insert into role(name) values ('ROLE_USER'),('ROLE_ADMIN'), ('ROLE_SUPER_ADMIN'), ('ROLE_BLOCKED');
insert into variable (created_at, created_by, updated_at, updated_by, acceptable_one_number, acceptable_two_number,
                      max_login_attempts, question_one_number, question_two_number, time_one_number, time_two_number)
values (now(), 1, now(), 1, 45, 22, 4, 60, 30, 60, 30);